from django.conf import settings
from django.contrib.auth.models import AbstractUser
from django.db import models
from django.utils.translation import gettext_lazy as _

from apps.currencies.constants import Currencies


class User(AbstractUser):
    email = models.EmailField(_("email address"), blank=False, unique=True)
    base_currency = models.CharField(
        _("base currency"),
        max_length=3,
        default=settings.DEFAULT_CURRENCY,
        help_text=_("The user's base currency"),
        choices=Currencies.choices,
    )

    class Meta:
        verbose_name = _("user")
        verbose_name_plural = _("users")

    def __str__(self):
        return self.username

    def __repr__(self):
        return "<User id={} username={} email={} date_joined={}>".format(
            self.pk, self.username, self.email, self.date_joined
        )
