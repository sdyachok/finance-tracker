from rest_framework.pagination import PageNumberPagination

from finance_tracker.api.viewsets import UserRestrictedViewSet

from .models import Transaction
from .serializers import TransactionSerializer


class TransactionPagination(PageNumberPagination):
    page_size = 50


class TransactionViewSet(UserRestrictedViewSet):
    """The transaction CRUD api"""

    queryset = Transaction.objects.order_by(
        "-transaction_date", "-created_at"
    ).select_related(
        "category",
        "category__parent",
        "account_from",
        "account_from__group",
        "account_to",
        "account_to__group",
    )
    serializer_class = TransactionSerializer
    pagination_class = TransactionPagination
