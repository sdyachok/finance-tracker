from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class TransactionsAppConfig(AppConfig):
    name = "apps.transactions"
    verbose_name = _("Transactions")

    def ready(self):
        from . import signals  # noqa
