from django.contrib import admin

from .models import Transaction


@admin.register(Transaction)
class TransactionAdmin(admin.ModelAdmin):
    date_hierarchy = "transaction_date"

    list_display = (
        "transaction_date",
        "category",
        "amount",
        "is_expense",
        "user",
        "created_at",
    )
    list_filter = ("transaction_date", "created_at")
    list_select_related = ("category", "user")
