from django.db.models.signals import pre_save, post_save, post_delete
from django.dispatch import receiver

from apps.currencies.services import get_currency_rate

from .models import Transaction


@receiver(pre_save, sender=Transaction, dispatch_uid="transaction-pre-save")
def transaction_pre_save(sender, instance: "Transaction", **kwargs):
    base_currency = instance.user.base_currency

    instance.base_amount_from = instance.amount_from
    if instance.has_expense_account():
        account_currency = instance.account_from.currency
        rate = get_currency_rate(
            account_currency, base_currency, instance.transaction_date
        )

        instance.base_amount_from = instance.amount_from * rate

    instance.base_amount_to = instance.amount_to
    if instance.has_income_account():
        account_currency = instance.account_to.currency
        rate = get_currency_rate(
            account_currency, base_currency, instance.transaction_date
        )

        instance.base_amount_to = instance.amount_to * rate


@receiver(post_save, sender=Transaction, dispatch_uid="transaction-post-save")
def transaction_post_save(sender, instance: "Transaction", **kwargs):
    instance.update_accounts()
    instance.update_budgets()


@receiver(post_delete, sender=Transaction, dispatch_uid="transaction-post-delete")
def transaction_post_delete(sender, instance: "Transaction", **kwargs):
    instance.update_accounts()
    instance.update_budgets()
