from django.db import models
from django.utils.translation import gettext_lazy as _

from finance_tracker.contrib.mixins import TimestampedModel


class Transaction(TimestampedModel):
    user = models.ForeignKey(
        "users.User",
        verbose_name=_("user"),
        on_delete=models.CASCADE,
        db_index=True,
        related_name="transactions",
    )
    transaction_date = models.DateField(_("date"), db_index=True)
    description = models.CharField(
        _("description"), max_length=200, blank=True, null=True
    )
    category = models.ForeignKey(
        "categories.Category",
        verbose_name=_("category"),
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
        default=None,
        db_index=True,
        related_name="transactions",
    )
    account_from = models.ForeignKey(
        "accounts.Account",
        verbose_name=_("expense account"),
        on_delete=models.CASCADE,
        blank=True,
        null=True,
        default=None,
        db_index=True,
        related_name="expenses",
    )
    amount_from = models.DecimalField(
        _("expense amount"), max_digits=15, decimal_places=2, blank=True, default=0
    )
    account_to = models.ForeignKey(
        "accounts.Account",
        verbose_name=_("income account"),
        on_delete=models.CASCADE,
        blank=True,
        null=True,
        default=None,
        db_index=True,
        related_name="incomes",
    )
    amount_to = models.DecimalField(
        _("income amount"), max_digits=15, decimal_places=2, blank=True, default=0
    )
    base_amount_from = models.DecimalField(
        _("expense in currency"),
        max_digits=15,
        decimal_places=2,
        blank=True,
        default=0,
        help_text=_("The expense amount in the user's base currency."),
    )
    base_amount_to = models.DecimalField(
        _("income in currency"),
        max_digits=15,
        decimal_places=2,
        blank=True,
        default=0,
        help_text=_("The income amount in the user's base currency."),
    )

    class Meta:
        verbose_name = _("transaction")
        verbose_name_plural = _("transactions")
        ordering = ("-transaction_date",)

    def __str__(self):
        return str(self.transaction_date)

    def __repr__(self):
        return "<Transaction id={} user={} date={} amount={}>".format(
            self.id, self.user_id, self.transaction_date, self.amount
        )

    @property
    def is_expense(self):
        return self.account_from_id is not None and self.account_to_id is None

    @property
    def is_income(self):
        return self.account_from_id is None and self.account_to_id is not None

    @property
    def is_transfer(self):
        return self.account_from_id is not None and self.account_to_id is not None

    @property
    def amount(self):
        if self.is_income:
            return self.amount_to

        return self.amount_from

    def has_expense_account(self):
        return self.account_from_id is not None

    def has_income_account(self):
        return self.account_to_id is not None

    def update_accounts(self):
        """Updates connected accounts"""
        if self.has_expense_account():
            self.account_from.calculate_current_amount()

        if self.has_income_account():
            self.account_to.calculate_current_amount()

    def update_budgets(self):
        """Updates user's budgets"""

        budgets = self.user.budgets.filter(is_active=True)
        for budget in budgets:
            budget.current.calculate_amounts()
