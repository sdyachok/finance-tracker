from rest_framework.serializers import ModelSerializer

from apps.accounts.models import Account
from apps.accounts.serializers import RelatedAccountSerializer
from apps.categories.models import Category
from apps.categories.serializers import RelatedCategorySerializer
from finance_tracker.api.fields import UserRestrictedRelatedField

from .models import Transaction


class TransactionSerializer(ModelSerializer):
    category = RelatedCategorySerializer(read_only=True)
    category_id = UserRestrictedRelatedField(
        source="category", queryset=Category.objects.all(), allow_null=True
    )

    account_from = RelatedAccountSerializer(read_only=True)
    account_from_id = UserRestrictedRelatedField(
        source="account_from", queryset=Account.objects.all(), allow_null=True
    )

    account_to = RelatedAccountSerializer(read_only=True)
    account_to_id = UserRestrictedRelatedField(
        source="account_to", queryset=Account.objects.all(), allow_null=True
    )

    class Meta:
        model = Transaction
        exclude = ["user"]
        read_only_fields = ["base_amount_from", "base_amount_to"]
