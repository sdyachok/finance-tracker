from django.core.management import BaseCommand

from apps.currencies.constants import Currencies
from apps.currencies.models import CurrencyRate
from apps.currencies.provider import get_latest_rates


class Command(BaseCommand):
    help = "Updates today's currency rate"

    def handle(self, *args, **options):
        objects = []
        for currency in Currencies.values:
            date, rates = get_latest_rates(currency)

            objects.extend(
                map(
                    lambda rate: CurrencyRate(
                        currency_from=currency,
                        currency_to=rate[0],
                        rate_date=date,
                        rate=rate[1],
                    ),
                    rates.items(),
                )
            )

        res = CurrencyRate.objects.bulk_create(
            objects,
            update_conflicts=True,
            update_fields=("rate", "updated_at"),
            unique_fields=("currency_from", "currency_to", "rate_date"),
        )

        self.stdout.write(self.style.SUCCESS(f"Successfully updated {len(res)} rates"))
