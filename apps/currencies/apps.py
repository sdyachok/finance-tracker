from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class CurrenciesAppConfig(AppConfig):
    name = "apps.currencies"
    verbose_name = _("Currencies")
