from django.db import models
from django.db.models.enums import ChoicesMeta
from django.utils.translation import gettext_lazy as _


class CurrenciesMeta(ChoicesMeta):
    @property
    def choices(cls):
        empty = [(None, cls.__empty__)] if hasattr(cls, "__empty__") else []
        return empty + [
            (member.value, f"{member.value} - {member.label}") for member in cls
        ]


class Currencies(models.TextChoices, metaclass=CurrenciesMeta):
    AUD = "AUD", _("Australian dollar")
    EUR = "EUR", _("Euro")
    PLN = "PLN", _("Polish złoty")
    UAH = "UAH", _("Ukrainian hryvnia")
    USD = "USD", _("US Dollar")
