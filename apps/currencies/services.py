import datetime as dt

from .models import CurrencyRate
from .provider import get_rate


def get_currency_rate(currency_from, currency_to, date=None):
    if currency_from == currency_to:
        return 1

    date = dt.date.today() if date is None else date

    try:
        currency_rate = CurrencyRate.objects.get(
            currency_from=currency_from, currency_to=currency_to, rate_date=date
        )
    except CurrencyRate.DoesNotExist:
        rate = get_rate(currency_from, currency_to, date)
        currency_rate = CurrencyRate.objects.create(
            currency_from=currency_from,
            currency_to=currency_to,
            rate_date=date,
            rate=rate,
        )

    return currency_rate.rate
