from decimal import Decimal

import httpx

from .constants import Currencies


BASE_URL = "https://api.exchangerate.host"


def get_rate(currency_from, currency_to, date):
    url = f"{BASE_URL}/convert"

    params = {
        "from": currency_from,
        "to": currency_to,
        "places": 6,
        "date": date,
        "amount": 1,
    }

    res = httpx.get(url, params=params).json()

    return Decimal(res["result"])


def get_latest_rates(base_currency):
    url = f"{BASE_URL}/latest"

    params = {
        "base": base_currency,
        "symbols": ",".join(Currencies.values),
        "places": 6,
    }

    res = httpx.get(url, params=params).json()

    return res["date"], res["rates"]
