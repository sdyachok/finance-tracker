from django.db import models
from django.utils.translation import gettext_lazy as _

from finance_tracker.contrib.mixins import TimestampedModel

from .constants import Currencies


class CurrencyRate(TimestampedModel):
    currency_from = models.CharField(
        _("currency from"), max_length=3, choices=Currencies.choices
    )
    currency_to = models.CharField(
        _("currency to"), max_length=3, choices=Currencies.choices
    )
    rate_date = models.DateField(_("date"))
    rate = models.DecimalField(
        _("currency rate"), max_digits=15, decimal_places=6, default=1
    )

    class Meta:
        verbose_name = _("currency rate")
        verbose_name_plural = _("currency rates")

        constraints = [
            models.UniqueConstraint(
                fields=("currency_from", "currency_to", "rate_date"),
                name="unq_currency_rate_on_date",
            )
        ]

    def __str__(self):
        return f"{self.currency_from}{self.currency_to}"

    def __repr__(self):
        return "<CurrencyRate from={} to={} date={} rate={}>".format(
            self.currency_from, self.currency_to, self.rate_date, self.rate
        )
