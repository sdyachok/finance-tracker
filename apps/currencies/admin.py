from django.contrib import admin

from .models import CurrencyRate


@admin.register(CurrencyRate)
class CurrencyRateAdmin(admin.ModelAdmin):
    date_hierarchy = "rate_date"

    list_display = (
        "currency_from",
        "currency_to",
        "rate_date",
        "rate",
        "created_at",
        "updated_at",
    )
    list_display_links = ("currency_from", "currency_to")
    list_filter = ("currency_from", "currency_to")

    ordering = ("-rate_date",)
