from finance_tracker.api.viewsets import UserRestrictedViewSet

from .models import AccountGroup, Account
from .serializers import AccountGroupSerializer, AccountSerializer


class AccountGroupViewSet(UserRestrictedViewSet):
    """The account group CRUD api"""

    queryset = AccountGroup.objects.all()
    serializer_class = AccountGroupSerializer


class AccountViewSet(UserRestrictedViewSet):
    """The account CRUD api"""

    queryset = Account.objects.all()
    serializer_class = AccountSerializer
