from django.db.models.signals import pre_save
from django.dispatch import receiver

from apps.currencies.services import get_currency_rate

from .models import Account


@receiver(pre_save, sender=Account, dispatch_uid="account-pre-save")
def account_pre_save(sender, instance: "Account", **kwargs):
    base_currency = instance.user.base_currency
    account_currency = instance.currency

    instance.base_opening_balance = instance.opening_balance
    instance.base_current_balance = instance.current_balance

    if account_currency != base_currency:
        # get currency rate for today
        rate = get_currency_rate(account_currency, base_currency)

        instance.base_opening_balance = instance.opening_balance * rate
        instance.base_current_balance = instance.current_balance * rate
