from rest_framework.serializers import ModelSerializer

from finance_tracker.api.fields import UserRestrictedRelatedField
from .models import AccountGroup, Account


class AccountGroupSerializer(ModelSerializer):
    class Meta:
        model = AccountGroup
        fields = ["id", "name"]


class RelatedAccountSerializer(ModelSerializer):
    group = AccountGroupSerializer(read_only=True)

    class Meta:
        model = Account
        fields = ["id", "name", "currency", "is_active", "group"]


class AccountSerializer(ModelSerializer):
    group = AccountGroupSerializer(read_only=True)
    group_id = UserRestrictedRelatedField(
        source="group", queryset=AccountGroup.objects.all(), allow_null=True
    )

    class Meta:
        model = Account
        exclude = ["user"]
        read_only_fields = ["base_opening_balance", "base_current_balance"]

    def create(self, validated_data):
        validated_data["current_balance"] = validated_data["opening_balance"]
        return super().create(validated_data)
