from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class AccountsAppConfig(AppConfig):
    name = "apps.accounts"
    verbose_name = _("Accounts")

    def ready(self):
        from . import signals  # noqa
