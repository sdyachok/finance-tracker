from django.contrib import admin

from .models import Account, AccountGroup


@admin.register(AccountGroup)
class AccountGroupAdmin(admin.ModelAdmin):
    list_display = ("name", "user", "created_at")
    list_select_related = ("user",)


@admin.register(Account)
class AccountAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "is_active",
        "currency",
        "current_balance",
        "user",
        "created_at",
    )
    list_filter = ("is_active", "currency", "created_at")
    list_select_related = ("user",)
