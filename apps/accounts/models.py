from django.conf import settings
from django.db import models
from django.utils.translation import gettext_lazy as _

from apps.currencies.constants import Currencies
from finance_tracker.contrib.mixins import TimestampedModel


class AccountGroup(TimestampedModel):
    user = models.ForeignKey(
        "users.User",
        on_delete=models.CASCADE,
        db_index=True,
        related_name="account_groups",
    )
    name = models.CharField(_("name"), max_length=120)

    class Meta:
        verbose_name = _("account group")
        verbose_name_plural = _("account groups")

    def __str__(self):
        return self.name

    def __repr__(self):
        return f"<AccountGroup id={self.pk} user={self.user_id} name={self.name}>"


class Account(TimestampedModel):
    user = models.ForeignKey(
        "users.User",
        verbose_name=_("user"),
        on_delete=models.CASCADE,
        db_index=True,
        related_name="accounts",
    )
    group = models.ForeignKey(
        AccountGroup,
        verbose_name=_("account group"),
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
        default=None,
        db_index=True,
        related_name="accounts",
    )
    name = models.CharField(_("name"), max_length=120)
    is_active = models.BooleanField(_("active"), default=True)
    currency = models.CharField(
        _("currency"),
        max_length=3,
        default=settings.DEFAULT_CURRENCY,
        choices=Currencies.choices,
    )
    opening_balance = models.DecimalField(
        _("opening balance"),
        max_digits=15,
        decimal_places=2,
        default=0,
        help_text=_("The account's opening balance"),
    )
    current_balance = models.DecimalField(
        _("current balance"),
        max_digits=15,
        decimal_places=2,
        default=0,
        help_text=_("The account's current balance"),
    )
    base_opening_balance = models.DecimalField(
        _("base opening balance"),
        max_digits=15,
        decimal_places=2,
        default=0,
        help_text=_("The account's opening balance in the user's base currency"),
    )
    base_current_balance = models.DecimalField(
        _("base current balance"),
        max_digits=15,
        decimal_places=2,
        default=0,
        help_text=_("The account's current balance in the user's base currency"),
    )

    class Meta:
        verbose_name = _("account")
        verbose_name_plural = _("accounts")

    def __str__(self):
        return self.name

    def __repr__(self):
        return "<Account id={} user={} name={} is_active={} created_at={}>".format(
            self.pk, self.user_id, self.name, self.is_active, self.created_at
        )

    def calculate_current_amount(self, save=True):
        total_income = (
            self.incomes.aggregate(total=models.Sum("amount_to")).get("total") or 0
        )

        total_expenses = (
            self.expenses.aggregate(total=models.Sum("amount_from")).get("total") or 0
        )

        self.current_balance = self.opening_balance + total_income - total_expenses

        if save:
            self.save()
