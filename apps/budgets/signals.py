from django.db.models.signals import post_save
from django.dispatch import receiver

from .models import Budget


@receiver(post_save, sender=Budget, dispatch_uid="budget-post-save")
def budget_post_save(sender, instance: "Budget", created: bool, **kwargs):
    budget_record = instance.get_current_record()
    budget_record.calculate_amounts()
