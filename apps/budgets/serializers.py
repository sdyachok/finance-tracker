from rest_framework.serializers import ModelSerializer

from .models import Budget, BudgetHistory


class BudgetHistorySerializer(ModelSerializer):
    class Meta:
        model = BudgetHistory
        fields = [
            "id",
            "start_date",
            "end_date",
            "planned_amount",
            "current_amount",
            "usage_percent",
        ]


class BudgetSerializer(ModelSerializer):
    current = BudgetHistorySerializer(read_only=True)

    class Meta:
        model = Budget
        fields = ["id", "name", "period", "is_active", "is_fixed", "amount", "current"]
