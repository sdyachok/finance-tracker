from django.contrib import admin

from .models import Budget, BudgetHistory


class BudgetHistoryInline(admin.TabularInline):
    model = BudgetHistory
    extra = 1


@admin.register(Budget)
class BudgetAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "period",
        "amount",
        "is_fixed",
        "is_active",
        "user",
        "created_at",
    )
    list_filter = ("period", "is_active", "is_fixed", "created_at")
    list_select_related = ("user",)

    inlines = (BudgetHistoryInline,)
