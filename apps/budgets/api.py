from finance_tracker.api.viewsets import UserRestrictedViewSet

from .models import Budget
from .serializers import BudgetSerializer


class BudgetViewSet(UserRestrictedViewSet):
    """The budget CRUD api"""

    queryset = Budget.objects.all()
    serializer_class = BudgetSerializer
