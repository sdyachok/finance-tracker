import datetime as dt

import pendulum
from django.db import models
from django.utils.translation import gettext_lazy as _

from finance_tracker.contrib.mixins import TimestampedModel


class BudgetPeriod(models.TextChoices):
    WEEK = "week", _("Week")
    MONTH = "month", _("Month")
    YEAR = "year", _("Year")


class Budget(TimestampedModel):
    user = models.ForeignKey(
        "users.User",
        verbose_name=_("user"),
        on_delete=models.CASCADE,
        db_index=True,
        related_name="budgets",
    )
    name = models.CharField(_("name"), max_length=120)
    period = models.CharField(
        _("budget period"), max_length=12, choices=BudgetPeriod.choices
    )
    is_active = models.BooleanField(_("active"), default=True)
    is_fixed = models.BooleanField(
        _("fixed amount"),
        default=False,
        help_text=_("Set this budget as a fixed-amount budget or percent of income"),
    )
    amount = models.DecimalField(
        _("budget amount"), max_digits=15, decimal_places=2, default=0
    )

    class Meta:
        verbose_name = _("budget")
        verbose_name_plural = _("budgets")

    def __str__(self):
        return self.name

    def __repr__(self):
        return "<Budget id={} user={} name={} period={} amount={} created={}>".format(
            self.pk, self.user_id, self.name, self.period, self.amount, self.created_at
        )

    @property
    def current(self) -> "BudgetHistory":
        return self.get_current_record()

    def get_current_record(self):
        start_date, end_date = self.get_time_period()

        budget_record, _ = self.budget_history.get_or_create(
            user=self.user, start_date=start_date, end_date=end_date
        )

        return budget_record

    def get_time_period(self, date=None):
        date = pendulum.today().date() if date is None else pendulum.instance(date)

        if self.period == BudgetPeriod.WEEK:
            start_date = date.start_of("week")
            end_date = date.end_of("week")
        elif self.period == BudgetPeriod.MONTH:
            start_date = date.start_of("month")
            end_date = date.end_of("month")
        else:
            start_date = date.start_of("year")
            end_date = date.end_of("year")

        return start_date, end_date


class BudgetHistory(TimestampedModel):
    user = models.ForeignKey(
        "users.User",
        verbose_name=_("user"),
        on_delete=models.CASCADE,
        db_index=True,
        related_name="budget_history",
    )
    budget = models.ForeignKey(
        Budget,
        verbose_name=_("budget"),
        on_delete=models.CASCADE,
        db_index=True,
        related_name="budget_history",
    )
    start_date = models.DateField(_("start date"), db_index=True)
    end_date = models.DateField(_("end date"))
    planned_amount = models.DecimalField(
        _("planned amount"), default=0, max_digits=15, decimal_places=2
    )
    current_amount = models.DecimalField(
        _("current amount"), default=0, max_digits=15, decimal_places=2
    )

    class Meta:
        verbose_name = _("budget record")
        verbose_name_plural = _("budget records")

        ordering = ["-start_date"]

        constraints = [
            models.UniqueConstraint(
                fields=("user", "budget", "start_date", "end_date"),
                name="unq_user_budget_period",
            )
        ]

    def __str__(self):
        return f"{self.start_date} - {self.end_date}"

    def __repr__(self):
        return "<BudgetHistory id={} user={} budget={} start={} planned={}>".format(
            self.pk, self.user_id, self.budget_id, self.start_date, self.planned_amount
        )

    @property
    def usage_percent(self):
        if not self.current_amount:
            return 0

        return round((self.planned_amount / self.current_amount) * 100, 2)

    def calculate_amounts(self, save=True):
        if self.budget.is_fixed:
            self.planned_amount = self.budget.amount
        else:
            income = self.calculate_income()
            self.planned_amount = income * (self.budget.amount / 100)

        self.current_amount = self.calculate_expenses()

        if save:
            self.save()

    def calculate_income(self):
        income = (
            self.user.transactions.filter(
                transaction_date__range=(self.start_date, self.end_date)
            )
            .filter(account_from__isnull=True)
            .aggregate(total=models.Sum("base_amount_to"))
            .get("total")
        )

        return income or 0

    def calculate_expenses(self):
        expenses = (
            self.user.transactions.filter(
                transaction_date__range=(self.start_date, self.end_date)
            )
            .filter(account_to__isnull=True)
            .aggregate(total=models.Sum("base_amount_from"))
            .get("total")
        )

        return expenses or 0
