from finance_tracker.api.viewsets import UserRestrictedViewSet

from .models import Category
from .serializers import CategorySerializer


class CategoryViewSet(UserRestrictedViewSet):
    """The category CRUD api"""

    queryset = Category.objects.select_related("parent").prefetch_related(
        "sub_categories"
    )
    serializer_class = CategorySerializer

    def get_queryset(self):
        qs = super().get_queryset()

        if self.action == "list":
            qs = qs.filter(parent__isnull=True)

        return qs
