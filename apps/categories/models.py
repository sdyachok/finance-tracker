from django.db import models
from django.utils.translation import gettext_lazy as _

from finance_tracker.contrib.mixins import TimestampedModel


class Category(TimestampedModel):
    user = models.ForeignKey(
        "users.User",
        verbose_name=_("user"),
        on_delete=models.CASCADE,
        db_index=True,
        related_name="categories",
    )
    parent = models.ForeignKey(
        "self",
        verbose_name=_("parent category"),
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
        db_index=True,
        related_name="sub_categories",
    )
    name = models.CharField(_("name"), max_length=50)
    is_expense = models.BooleanField(_("expense category"), default=True)

    class Meta:
        verbose_name = _("category")
        verbose_name_plural = _("categories")

    def __str__(self):
        return self.name

    def __repr__(self):
        return "<Category id={} user={} parent={} name={} is_expense={}>".format(
            self.pk, self.user_id, self.parent_id, self.name, self.is_expense
        )

    @property
    def full_name(self):
        return f"{self.parent.name} / {self.name}" if self.parent_id else self.name
