from django.contrib import admin

from .models import Category


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    search_fields = ("name",)

    list_display = ("name", "parent", "is_expense", "user", "created_at")
    list_filter = ("is_expense", "created_at")
    list_select_related = ("parent", "user")
