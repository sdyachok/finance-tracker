from rest_framework.fields import CharField
from rest_framework.serializers import ModelSerializer

from finance_tracker.api.fields import UserRestrictedRelatedField
from .models import Category


class SubcategorySerializer(ModelSerializer):
    class Meta:
        model = Category
        fields = ["id", "name", "full_name"]


class RelatedCategorySerializer(ModelSerializer):
    parent = SubcategorySerializer(read_only=True)

    class Meta:
        model = Category
        fields = ["id", "parent", "name", "full_name", "is_expense"]


class CategorySerializer(ModelSerializer):
    parent = SubcategorySerializer(read_only=True)
    parent_id = UserRestrictedRelatedField(
        source="parent",
        queryset=Category.objects.filter(parent__isnull=True),
        allow_null=True,
    )
    sub_categories = SubcategorySerializer(many=True, read_only=True)
    full_name = CharField(read_only=True)

    class Meta:
        model = Category
        exclude = ["user"]
