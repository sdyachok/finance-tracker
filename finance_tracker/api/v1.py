from djoser.views import UserViewSet
from rest_framework.routers import DefaultRouter

from apps.accounts.api import AccountGroupViewSet, AccountViewSet
from apps.categories.api import CategoryViewSet
from apps.transactions.api import TransactionViewSet
from apps.budgets.api import BudgetViewSet

router = DefaultRouter()

router.register("users", UserViewSet)
router.register("account-groups", AccountGroupViewSet, basename="account-group")
router.register("accounts", AccountViewSet, basename="account")
router.register("categories", CategoryViewSet)
router.register("transactions", TransactionViewSet)
router.register("budgets", BudgetViewSet)
