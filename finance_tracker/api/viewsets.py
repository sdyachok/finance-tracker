from rest_framework.viewsets import ModelViewSet


class UserRestrictedViewSet(ModelViewSet):
    def get_queryset(self):
        return super().get_queryset().filter(user=self.request.user)

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)
