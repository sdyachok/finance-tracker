from rest_framework.relations import PrimaryKeyRelatedField


class UserRestrictedRelatedField(PrimaryKeyRelatedField):
    def get_queryset(self):
        qs = super().get_queryset()
        return qs.filter(user=self.context["request"].user)
