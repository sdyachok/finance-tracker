import { useAuthStore } from "@/stores/auth";
import api from "@/api";
import { TOKEN_STORAGE_KEY } from "@/constants";

export function useAuth() {
  const store = useAuthStore();

  const loadUser = async () => {
    const sessionToken = sessionStorage.getItem(TOKEN_STORAGE_KEY);
    const token = localStorage.getItem(TOKEN_STORAGE_KEY);

    if (!sessionToken && !token) {
      store.setUser(null);
      return;
    }

    try {
      store.setLoading(true);
      store.setError(null);

      const user = await api.auth.me();
      if (user) {
        store.setUser(user);
      }
    } catch (e) {
      store.setUser(null);
    } finally {
      store.setLoading(false);
    }
  };

  const login = async (data: LoginData, rememberMe = true) => {
    try {
      store.setLoading(true);
      store.setError(null);

      const token = await api.auth.getToken(data);

      const storage = rememberMe ? localStorage : sessionStorage;
      storage.setItem(TOKEN_STORAGE_KEY, token.auth_token);

      const user = await api.auth.me();
      store.setUser(user);

      return user;
    } catch (e) {
      store.setError((e as Error).message);
    } finally {
      store.setLoading(false);
    }

    return null;
  };

  const register = async (data: RegisterData) => {
    try {
      store.setLoading(true);
      store.setError(null);

      const user = await api.auth.register(data);
      const token = await api.auth.getToken({ username: data.username, password: data.password });

      sessionStorage.setItem(TOKEN_STORAGE_KEY, token.auth_token);
      store.setUser(user);

      // todo: redirect to confirmation/activation page
      return user;
    } catch (e) {
      store.setError((e as Error).message);
    } finally {
      store.setLoading(false);
    }

    return null;
  };

  const logout = async () => {
    try {
      await api.auth.logout();

      sessionStorage.removeItem(TOKEN_STORAGE_KEY);
      localStorage.removeItem(TOKEN_STORAGE_KEY);

      store.setUser(null);

      return true;
    } catch (e) {
      store.setError((e as Error).message);
    }

    return false;
  };

  return { login, register, logout, loadUser, state: store };
}
