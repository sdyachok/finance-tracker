import api from "@/api";
import { useAccountGroupsStore } from "@/stores/accountGroups";

export function useGroups() {
  const store = useAccountGroupsStore();

  const load = async () => {
    try {
      store.setLoading(true);
      store.setError(null);

      const result = await api.groups.getAccountGroups();
      store.setData(result);
    } catch (e) {
      store.setError((e as Error).message);
    } finally {
      store.setLoading(false);
    }
  };

  const create = async (data: AccountGroupData) => {
    try {
      store.setError(null);

      const group = await api.groups.create(data);

      store.addGroup(group);

      return group;
    } catch (e) {
      store.setError((e as Error).message);
    }

    return null;
  };

  const update = async (id: number, data: AccountGroupData) => {
    try {
      const group = await api.groups.update(id, data);

      store.updateGroup(group);

      return group;
    } catch (e) {
      store.setError((e as Error).message);
    }

    return null;
  };

  const remove = async (id: number) => {
    try {
      await api.groups.remove(id);

      store.removeGroup(id);

      return true;
    } catch (e) {
      store.setError((e as Error).message);
    }

    return false;
  };

  return { state: store, load, create, update, remove };
}
