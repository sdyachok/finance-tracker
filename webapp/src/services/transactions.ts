import api from "@/api";
import { useTransactionsStore } from "@/stores/transactions";

export function useTransactions() {
  const store = useTransactionsStore();

  const load = async (trackLoading = true) => {
    try {
      if (trackLoading) store.setLoading(true);

      store.setError(null);
      store.setData(await api.transactions.list());
    } catch (e) {
      store.setError((e as Error).message);
    } finally {
      if (trackLoading) store.setLoading(false);
    }
  };

  const create = async (data: TransactionData) => {
    try {
      store.setError(null);

      const category = await api.transactions.create(data);

      store.add(category);

      return category;
    } catch (e) {
      store.setError((e as Error).message);
    }

    return null;
  };

  const update = async (id: number, data: TransactionData) => {
    try {
      const category = await api.transactions.update(id, data);

      store.update(id, category);

      return category;
    } catch (e) {
      store.setError((e as Error).message);
    }

    return null;
  };

  const remove = async (id: number) => {
    try {
      await api.transactions.remove(id);

      store.remove(id);

      return true;
    } catch (e) {
      store.setError((e as Error).message);
    }

    return false;
  };

  return { state: store, load, create, update, remove };
}
