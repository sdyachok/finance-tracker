import api from "@/api";
import { useCategoriesStore } from "@/stores/categories";

export function useCategories() {
  const store = useCategoriesStore();

  const load = async (trackLoading = true) => {
    try {
      if (trackLoading) store.setLoading(true);

      store.setError(null);
      store.setData(await api.categories.list());
    } catch (e) {
      store.setError((e as Error).message);
    } finally {
      if (trackLoading) store.setLoading(false);
    }
  };

  const create = async (data: CategoryData) => {
    try {
      store.setError(null);

      const category = await api.categories.create(data);

      store.add(category);

      return category;
    } catch (e) {
      store.setError((e as Error).message);
    }

    return null;
  };

  const update = async (id: number, data: CategoryData) => {
    try {
      const category = await api.categories.update(id, data);

      store.update(id, category);

      return category;
    } catch (e) {
      store.setError((e as Error).message);
    }

    return null;
  };

  const remove = async (id: number) => {
    try {
      await api.categories.remove(id);

      store.remove(id);

      return true;
    } catch (e) {
      store.setError((e as Error).message);
    }

    return false;
  };

  return { state: store, load, create, update, remove };
}
