import api from "@/api";
import { useAccountsStore } from "@/stores/accounts";

export function useAccounts() {
  const store = useAccountsStore();

  const load = async (showLoader = true) => {
    try {
      if (showLoader) store.setLoading(true);
      store.setError(null);

      const result = await api.accounts.list();
      store.setData(result);
    } catch (e) {
      store.setError((e as Error).message);
    } finally {
      if (showLoader) store.setLoading(false);
    }
  };

  const create = async (data: AccountData) => {
    try {
      store.setError(null);

      const account = await api.accounts.create(data);

      store.addAccount(account);

      return account;
    } catch (e) {
      store.setError((e as Error).message);
    }

    return null;
  };

  const update = async (accountId: number, data: AccountData) => {
    try {
      const account = await api.accounts.update(accountId, data);

      store.updateAccount(accountId, account);

      return account;
    } catch (e) {
      store.setError((e as Error).message);
    }

    return null;
  };

  const remove = async (id: number) => {
    try {
      await api.accounts.remove(id);

      store.removeAccount(id);

      return true;
    } catch (e) {
      store.setError((e as Error).message);
    }

    return false;
  };

  return { state: store, load, create, update, remove };
}
