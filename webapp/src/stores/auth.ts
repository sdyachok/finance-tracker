import { defineStore } from "pinia";
import { TOKEN_STORAGE_KEY } from "@/constants";

export const useAuthStore = defineStore({
  id: "auth",

  state: (): AuthState => {
    const sessionToken = sessionStorage.getItem(TOKEN_STORAGE_KEY);
    const token = localStorage.getItem(TOKEN_STORAGE_KEY);

    return {
      isAuthenticated: !!sessionToken || !!token,
      isLoading: false,
      error: null,
      user: null,
    };
  },

  actions: {
    setUser(user: User | null) {
      this.user = user;
      this.isAuthenticated = !!user;
    },

    setError(error: string | null) {
      this.error = error;
    },

    setLoading(isLoading: boolean) {
      this.isLoading = isLoading;
    },
  },
});
