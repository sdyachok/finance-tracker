import { defineStore } from "pinia";
import { mapTransactionType } from "@/mappers/transactions";

export const useTransactionsStore = defineStore({
  id: "transactions",

  state: (): TransactionsState => ({
    isLoading: false,
    error: null,
    data: {
      count: 0,
      next: null,
      previous: null,
      results: [],
    },
  }),

  getters: {
    transactions(state): Transaction[] {
      return state.data.results.sort((t1, t2) => (t1.transaction_date > t2.transaction_date ? 1 : 0));
    },
  },

  actions: {
    setData(data: PaginatedData<Transaction>) {
      this.data = {
        ...data,
        results: data.results.map(mapTransactionType),
      };
    },

    transaction(id: number) {
      return this.data.results.find((transaction) => transaction.id === id);
    },

    add(value: Transaction) {
      this.data.results.push(mapTransactionType(value));
      this.data.count += 1;
    },

    update(id: number, updatedTransaction: Transaction) {
      this.data.results = this.data.results.map((transaction) =>
        transaction.id === id ? mapTransactionType(updatedTransaction) : transaction
      );
    },

    remove(id: number) {
      this.data.results = this.data.results.filter((transaction) => transaction.id !== id);
      this.data.count -= 1;
    },

    setLoading(isLoading: boolean) {
      this.isLoading = isLoading;
    },

    setError(error: string | null) {
      this.error = error;
    },
  },
});
