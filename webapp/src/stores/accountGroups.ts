import { defineStore } from "pinia";

export const useAccountGroupsStore = defineStore({
  id: "groups",

  state: (): ResultSate<AccountGroup> => ({
    isLoading: false,
    error: null,
    data: [],
  }),

  actions: {
    setData(data: AccountGroup[]) {
      this.data = data;
    },

    addGroup(group: AccountGroup) {
      this.data.push(group);
    },

    updateGroup(updatedGroup: AccountGroup) {
      this.data = this.data.map((group) => (group.id === updatedGroup.id ? updatedGroup : group));
    },

    removeGroup(id: number) {
      this.data = this.data.filter((group) => group.id !== id);
    },

    setLoading(isLoading: boolean) {
      this.isLoading = isLoading;
    },

    setError(error: string | null) {
      this.error = error;
    },
  },
});
