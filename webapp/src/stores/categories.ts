import { defineStore } from "pinia";

export const useCategoriesStore = defineStore({
  id: "categories",

  state: (): ResultSate<Category> => ({
    isLoading: false,
    error: null,
    data: [],
  }),

  getters: {
    expenses(state): Category[] {
      return state.data.filter((category) => category.is_expense);
    },

    incomes(state): Category[] {
      return state.data.filter((category) => !category.is_expense);
    },
  },

  actions: {
    setData(data: Category[]) {
      this.data = data;
    },

    category(id: number) {
      return this.data.find((category) => category.id === id);
    },

    add(value: Category) {
      this.data.push(value);
    },

    update(id: number, updatedCategory: Category) {
      this.data = this.data.map((category) => (category.id === id ? updatedCategory : category));
    },

    remove(id: number) {
      this.data = this.data.filter((category) => category.id !== id);
    },

    setLoading(isLoading: boolean) {
      this.isLoading = isLoading;
    },

    setError(error: string | null) {
      this.error = error;
    },
  },
});
