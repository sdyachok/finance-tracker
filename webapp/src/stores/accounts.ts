import { defineStore } from "pinia";

export const useAccountsStore = defineStore({
  id: "accounts",

  state: (): ResultSate<Account> => ({
    isLoading: false,
    error: null,
    data: [],
  }),

  getters: {
    groupedAccounts(state) {
      return state.data.reduce((groups, acc) => {
        const group_id = acc.group_id ?? 0;

        groups[group_id] = groups[group_id] ?? [];
        groups[group_id].push(acc);

        return groups;
      }, {} as { [key: number]: Account[] });
    },
  },

  actions: {
    setData(data: Account[]) {
      this.data = data;
    },

    addAccount(data: Account) {
      this.data.push(data);
    },

    updateAccount(accountId: number, updatedAccount: Account) {
      this.data = this.data.map((account) => (account.id === accountId ? updatedAccount : account));
    },

    removeAccount(accountId: number) {
      this.data = this.data.filter((account) => account.id !== accountId);
    },

    setLoading(isLoading: boolean) {
      this.isLoading = isLoading;
    },

    setError(error: string | null) {
      this.error = error;
    },
  },
});
