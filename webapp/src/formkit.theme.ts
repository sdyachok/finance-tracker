import { generateClasses } from "@formkit/themes";

const defaultTextInputClasses = {
  label: "block text-sm font-medium text-gray-700",
  inner: "mt-1",
  input:
    "block w-full appearance-none rounded-md border border-gray-300 px-3 py-2 placeholder-gray-400 shadow-sm focus:border-sky-500 focus:outline-none focus:ring-sky-500 sm:text-sm formkit-invalid:ring-red-500 formkit-invalid:border-red-500",
};

export default generateClasses({
  global: {
    help: "text-xs text-gray-500",
    messages: "mt-1",
    message: "text-red-500 text-sm",
  },
  date: defaultTextInputClasses,
  email: defaultTextInputClasses,
  text: defaultTextInputClasses,
  textarea: defaultTextInputClasses,
  number: defaultTextInputClasses,
  password: defaultTextInputClasses,
  checkbox: {
    input: "h-4 w-4 rounded border-gray-300 text-sky-600 focus:ring-sky-500",
    label: "ml-2 block text-sm text-gray-900",
    inner: "inline-flex",
    wrapper: "flex items-center",
    help: "ml-6 text-sm text-gray-500",
  },
  select: defaultTextInputClasses,
});
