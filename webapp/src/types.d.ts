declare interface LoginData {
  username: string;
  password: string;
}

declare interface RegisterData extends LoginData {
  email: string;
}

declare interface TokenResponse {
  auth_token: string;
}

declare interface PaginatedData<T> {
  count: number;
  next: string | null;
  previous: string | null;
  results: T[];
}

declare interface User {
  id: number;
  username: string;
  email: string;
}

declare interface AccountGroup {
  id: number;
  name: string;
}

declare type AccountGroupData = Omit<AccountGroup, "id">;

declare interface Account {
  id: number;
  group: AccountGroup | null;
  group_id: number | null;
  name: string;
  currency: string;
  is_active: boolean;
  opening_balance: string | number;
  current_balance: string;
  base_opening_balance: string;
  base_current_balance: string;
}

declare type RelatedAccount = Pick<Account, "id" | "name" | "currency" | "is_active" | "group">;

declare type AccountData = Omit<
  Account,
  "id" | "group" | "current_balance" | "base_current_balance" | "base_opening_balance"
>;

declare interface SubCategory {
  id: number;
  name: string;
  full_name: string;
}

declare interface Category extends SubCategory {
  parent: SubCategory | null;
  parent_id: number | null;
  is_expense: boolean;
  sub_categories: SubCategory[] | null;
  created_at: string;
  updated_at: string;
}

declare type CategoryData = Pick<Category, "name" | "parent_id" | "is_expense">;

declare type RelatedCategory = Pick<Category, "id" | "parent" | "name" | "full_name" | "is_expense">;

declare interface Transaction {
  id: number;

  category: RelatedCategory | null;
  category_id: number | null;

  account_from: RelatedAccount | null;
  account_from_id: number | null;
  amount_from: number;
  base_amount_from: number;

  account_to: RelatedAccount | null;
  account_to_id: number | null;
  amount_to: number;
  base_amount_to: number;

  transaction_date: string;
  description: string | null;
  type: "expense" | "income" | "transfer";
}

declare type TransactionData = Pick<
  Transaction,
  "transaction_date" | "description" | "category_id" | "account_from_id" | "amount_from" | "account_to_id" | "amount_to"
>;

declare interface AuthState {
  isAuthenticated: boolean;
  isLoading: boolean;
  error: string | null;
  user: User | null;
}

declare interface ResultSate<T> {
  isLoading: boolean;
  error: string | null;
  data: T[];
}

declare interface TransactionsState {
  isLoading: boolean;
  error: string | null;
  data: PaginatedData<Transaction>;
}
