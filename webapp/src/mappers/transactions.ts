import { TRANSACTION_TYPE_EXPENSE, TRANSACTION_TYPE_INCOME, TRANSACTION_TYPE_TRANSFER } from "@/constants";

import type { TransactionType } from "@/constants";

export const mapTransactionType = (transaction: Transaction): Transaction => {
  let transactionType = TRANSACTION_TYPE_EXPENSE;

  if (transaction.account_from_id && transaction.account_to_id) {
    transactionType = TRANSACTION_TYPE_TRANSFER;
  } else if (transaction.account_to_id && !transaction.account_from_id) {
    transactionType = TRANSACTION_TYPE_INCOME;
  }

  return { ...transaction, type: transactionType as TransactionType };
};
