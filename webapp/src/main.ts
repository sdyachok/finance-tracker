import { createApp } from "vue";
import { createPinia } from "pinia";
import { plugin as FormKitPlugin, defaultConfig as FormKitConfig } from "@formkit/vue";

import App from "./App.vue";
import router from "./router";

import formkitClasses from "./formkit.theme";

import "./assets/main.css";

const app = createApp(App);

app.use(createPinia());
app.use(router);
app.use(
  FormKitPlugin,
  FormKitConfig({
    config: {
      classes: formkitClasses,
    },
  })
);

app.mount("#app");
