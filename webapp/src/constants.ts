export const TOKEN_STORAGE_KEY = "ft:token";

export const CURRENCIES = [
  { label: "AUD - Australian dollar", value: "AUD" },
  { label: "EUR - Euro", value: "EUR" },
  { label: "PLN - Polish złoty", value: "PLN" },
  { label: "UAH - Ukrainian hryvnia", value: "UAH" },
  { label: "USD - US Dollar", value: "USD" },
];

export const TRANSACTION_TYPE_EXPENSE = "expense";
export const TRANSACTION_TYPE_INCOME = "income";
export const TRANSACTION_TYPE_TRANSFER = "transfer";

export type TransactionType =
  | typeof TRANSACTION_TYPE_EXPENSE
  | typeof TRANSACTION_TYPE_INCOME
  | typeof TRANSACTION_TYPE_TRANSFER;
