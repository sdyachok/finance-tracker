import { apiBase } from "./base";

const baseUrl = "/v1/transactions/";

const list = () => apiBase.get<PaginatedData<Transaction>>(baseUrl).then((res) => res.data);

const detail = (id: number) => apiBase.get<Transaction>(`${baseUrl}${id}`).then((res) => res.data);

const create = async (data: TransactionData) => apiBase.post<Transaction>(baseUrl, data).then((res) => res.data);

const update = async (id: number, data: TransactionData) =>
  apiBase.put<Transaction>(`${baseUrl}${id}/`, data).then((res) => res.data);

const remove = async (id: number) => apiBase.delete(`${baseUrl}${id}/`).then((res) => res.data);

export const transactionsApi = { list, detail, create, update, remove };
