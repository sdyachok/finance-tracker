import { apiBase } from "./base";

const getAccountGroups = () => apiBase.get<AccountGroup[]>("/v1/account-groups/").then((res) => res.data);

const create = async (data: AccountGroupData) =>
  apiBase.post<AccountGroup>("/v1/account-groups/", data).then((res) => res.data);

const update = async (id: number, data: AccountGroupData) =>
  apiBase.put<AccountGroup>(`/v1/account-groups/${id}/`, data).then((res) => res.data);

const remove = async (id: number) => apiBase.delete(`/v1/account-groups/${id}/`).then((res) => res.data);

export const groupsApi = { getAccountGroups, create, update, remove };
