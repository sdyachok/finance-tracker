import { apiBase } from "./base";

const getToken = async (data: LoginData) =>
  apiBase.post<TokenResponse>("/auth/token/login/", data).then((res) => res.data);

const register = async (data: RegisterData) => apiBase.post<User>("/v1/users/", data).then((res) => res.data);

const me = async () => apiBase.get<User>("/v1/users/me/").then((res) => res.data);

const logout = async () => apiBase.post("/auth/token/logout/").then((res) => res.data);

export const authApi = { getToken, register, me, logout };
