import { apiBase } from "./base";

const list = () => apiBase.get<Account[]>("/v1/accounts/").then((res) => res.data);

const create = async (data: AccountData) => apiBase.post<Account>("/v1/accounts/", data).then((res) => res.data);

const update = async (accountId: number, data: AccountData) =>
  apiBase.put<Account>(`/v1/accounts/${accountId}/`, data).then((res) => res.data);

const remove = async (accountId: number) => apiBase.delete(`/v1/accounts/${accountId}/`).then((res) => res.data);

export const accountsApi = { list, create, update, remove };
