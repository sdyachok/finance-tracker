import { apiBase } from "./base";

const baseUrl = "/v1/categories/";

const list = () => apiBase.get<Category[]>(baseUrl).then((res) => res.data);

const detail = (id: number) => apiBase.get<Category>(`${baseUrl}${id}`).then((res) => res.data);

const create = async (data: CategoryData) => apiBase.post<Category>(baseUrl, data).then((res) => res.data);

const update = async (id: number, data: CategoryData) =>
  apiBase.put<Category>(`${baseUrl}${id}/`, data).then((res) => res.data);

const remove = async (id: number) => apiBase.delete(`${baseUrl}${id}/`).then((res) => res.data);

export const categoriesApi = { list, detail, create, update, remove };
