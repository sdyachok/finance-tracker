import axios from "axios";
import { TOKEN_STORAGE_KEY } from "@/constants";

export const apiBase = axios.create({
  baseURL: "/api",
});

apiBase.interceptors.request.use((config) => {
  const sessionToken = sessionStorage.getItem(TOKEN_STORAGE_KEY);
  const token = localStorage.getItem(TOKEN_STORAGE_KEY);

  if (sessionToken || token) {
    if (!config.headers) {
      config.headers = {};
    }

    config.headers["Authorization"] = `Token ${sessionToken ?? token}`;
  }

  return config;
});
