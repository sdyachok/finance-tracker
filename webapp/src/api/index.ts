import { accountsApi } from "./accounts";
import { authApi } from "./auth";
import { categoriesApi } from "./categories";
import { groupsApi } from "./groups";
import { transactionsApi } from "./transactions";

export default {
  auth: authApi,
  groups: groupsApi,
  accounts: accountsApi,
  categories: categoriesApi,
  transactions: transactionsApi,
};
