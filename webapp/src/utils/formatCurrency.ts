export default function formatCurrency(amount: number | string, currency: string) {
  const formatter = new Intl.NumberFormat(undefined, {
    style: "currency",
    currency,
    minimumFractionDigits: 2,
  });

  return formatter.format(parseFloat(amount.toString()));
}
