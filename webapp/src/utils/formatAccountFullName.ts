export default function formatAccountFullName(account: Account | RelatedAccount) {
  const bits = [];

  if (account.group) {
    bits.push(account.group.name);
  }
  bits.push(account.name);

  return bits.join(" / ");
}
