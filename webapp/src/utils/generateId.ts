export default function (name?: string) {
  return name ? `id-${name}` : "field-" + (Math.random() * 1000 + 1000).toFixed();
}
