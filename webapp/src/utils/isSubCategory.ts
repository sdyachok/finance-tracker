export default function isSubCategory(value: Category | SubCategory): value is SubCategory {
  return (<Category>value).parent_id === undefined;
}
