declare module "notiwind" {
  import type { Component } from "vue";

  type NotificationType = { title: string; text: string; type?: string };

  const notify: (notification: NotificationType, timeout: number) => () => void;

  const Notification: Component;
  const NotificationGroup: Component;
}
