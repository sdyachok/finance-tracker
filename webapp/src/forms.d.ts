declare type SelectOption = { label: string; value: number | string };
declare type SelectOptions = SelectOption[];
